FROM webdevops/php-apache:ubuntu-16.04
    
# Add APCu 
# Source: https://launchpad.net/ubuntu/+source/php-apcu/5.1.3+4.0.10-1build1/+build/9538534/+files/php-apcu_5.1.3+4.0.10-1build1_amd64.deb
COPY teufels/   /opt/docker/teufels/
RUN dpkg -i /opt/docker/teufels/apcu/php-apcu_5.1.3+4.0.10-1build1_amd64.deb
RUN echo "extension=apcu.so\napc.enable_cli=1\napc.shm_size = 512M\napc.ttl=7200\napc.user_ttl=7200" > /etc/php/7.0/mods-available/apcu.ini 

#Add Ioncube Loader (beta)
# Source: https://www.ioncube.com/php7-linux-x86-64-beta8.tgz
RUN mkdir -p /usr/local/ioncube \
    && cp /opt/docker/teufels/ioncube/ioncube_loader_lin_x86-64_7.0b8.so /usr/local/ioncube \
    && chmod 644 /usr/local/ioncube/ioncube_loader_lin_x86-64_7.0b8.so \
    && echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_x86-64_7.0b8.so" > /etc/php/7.0/mods-available/ioncube.ini \
    && ln -s /etc/php/7.0/mods-available/ioncube.ini /etc/php/7.0/fpm/conf.d/00-ioncube.ini \
    && ln -s /etc/php/7.0/mods-available/ioncube.ini /etc/php/7.0/cli/conf.d/00-ioncube.ini 
    
# Add Gulp
RUN touch /etc/apt/sources.list.d/nodesource.list \
    && echo "deb https://deb.nodesource.com/node_6.x xenial main" >> /etc/apt/sources.list.d/nodesource.list \
    && echo "deb-src https://deb.nodesource.com/node_6.x xenial main" >> /etc/apt/sources.list.d/nodesource.list \
    && curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
RUN apt-get update
RUN apt-get install -y php-xdebug
RUN apt-get install xz-utils
RUN apt-get install libxrender1
RUN apt-get install -y build-essential
RUN apt-get install -y nodejs
RUN npm install -g gulp \
 && npm install --save-dev gulp \
 && npm install -global less \
 && npm install gulp-less \
 && npm install -g node-sass \
 && npm install gulp-sass --save-dev \
 && npm install gulp-clean-css --save-dev \
 && npm install --save-dev gulp-autoprefixer \
 && npm install --save-dev gulp-wrap \
 && npm install --save-dev gulp-tap \
 && npm install --save-dev gulp-file-include \
 && npm install gulp-concat gulp-rename gulp-uglify --save-dev \
 && npm install --save-dev gulp-util \
 && npm install --save-dev gulp-debug

RUN apt-get update
RUN apt-get install -y zsh

RUN chsh -s /bin/zsh
RUN chsh -s /bin/zsh application
# Install Oh My Zsh
RUN git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh \
    && cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc \
    && chsh -s /bin/zsh

 # Add wkhtmltopdf # http://download.gna.org/wkhtmltopdf/obsolete/linux/wkhtmltopdf-0.10.0_rc1-static-amd64.tar.lzma   
#RUN wget https://downloads.wkhtmltopdf.org/obsolete/linux/wkhtmltopdf-0.10.0_rc1-static-amd64.tar.lzma \
 RUN tar --lzma -xf /opt/docker/teufels/wkhtmltopdf/wkhtmltopdf-0.10.0_rc1-static-amd64.tar.lzma \
 && mv wkhtmltopdf-amd64 /opt/docker/teufels/wkhtmltopdf/ \
 && install /opt/docker/teufels/wkhtmltopdf/wkhtmltopdf-amd64 /usr/local/bin/wkhtmltopdf
 #&& rm -rf wkhtmltopdf-0.10.0_rc1-static-amd64.tar.lzma
 
 # Install services
RUN /usr/local/bin/apt-install \
        supervisor \
        syslog-ng \
        syslog-ng-core \
        logrotate \
        openssh-server \
        mysql-client \
        cron \
        dnsmasq \
        postfix

# Install common tools
RUN /usr/local/bin/apt-install \
        lsb-release \
        sudo \
        zip \
        unzip \
        bzip2 \
        wget \
        curl \
        net-tools \
        moreutils \
        dnsutils \
        openssh-client \
        rsync \
        git \
        nano \
        vim